import { ToDoListLightTheme } from "../../Themes/ToDoListLightTheme";
import {
  ADD_TASK,
  CHANGE_THEME,
  DELETE_TASK,
  DONE_TASK,
  EDIT_TASK,
  UPDATE_TASK,
} from "./../constants/ToDoListConstants";
import { arrTheme } from "./../../Themes/ThemeManager";

const initialState = {
  theme: ToDoListLightTheme,
  taskList: [
    { id: "task-1", taskName: "Task 1", done: true },
    { id: "task-2", taskName: "Task 2", done: false },
    { id: "task-3", taskName: "Task 3", done: true },
    { id: "task-4", taskName: "Task 4", done: false },
  ],
  taskEdit: { id: "-1", taskName: "", done: false },
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_TASK: {
      //Kiểm tra rỗng
      if (action.newTask.taskName.trim() === "") {
        alert("Task name is required");
        return { ...state };
      }
      //Kiểm tra tồn tại
      let taskListUpdate = [...state.taskList];
      let index = taskListUpdate.findIndex(
        (task) => task.taskName === action.newTask.taskName
      );
      if (index !== -1) {
        alert("Task name đã tồn tại !");
        return { ...state };
      }
      taskListUpdate.push(action.newTask);

      //Handle xong thì gán taskList mới vào tast list
      state.taskList = taskListUpdate;
      return { ...state };
    }
    case CHANGE_THEME: {
      //Tìm theme dựa vào  action.themeId được chọn
      let index = arrTheme.findIndex((theme) => theme.id == action.payload);
      if (index != -1) {
        state.theme = arrTheme[index].theme;
      }
      return { ...state };
    }
    case DONE_TASK: {
      let taskListUpdate = [...state.taskList];
      let index = taskListUpdate.findIndex((task) => task.id == action.payload);
      if (index !== -1) {
        taskListUpdate[index].done = true;
      }

      state.taskList = taskListUpdate;
      return { ...state };
    }
    case DELETE_TASK: {
      let taskListUpdate = [...state.taskList];
      taskListUpdate = taskListUpdate.filter(
        (task) => task.id !== action.payload
      );
      return { ...state, taskList: taskListUpdate };
    }
    case EDIT_TASK: {
      return { ...state, taskEdit: action.payload };
    }
    case UPDATE_TASK: {
      state.taskEdit = { ...state.taskEdit, taskName: action.payload };

      let taskListUpdate = [...state.taskList];
      let index = taskListUpdate.findIndex(
        (task) => task.id === state.taskEdit.id
      );

      if (index !== -1) {
        taskListUpdate[index] = state.taskEdit;
      }
      state.taskList = taskListUpdate;
      state.taskEdit = { id: "-1", taskName: "", done: false };
      return { ...state };
    }
    default:
      return state;
  }
};
