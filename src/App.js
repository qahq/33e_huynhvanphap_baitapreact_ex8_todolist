import logo from './logo.svg';
import './App.css';
import TodoList from './BaitapStyleComponent/TodoList';

function App() {
  return (
    <div className="App">
     <TodoList/>
    </div>
  );
}

export default App;
